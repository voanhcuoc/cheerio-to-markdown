var _ = require('lodash')
var S = require('string')
var commonmarkRuleSet = require('./rule/commonmark')
var extension = require('./rule/extension')
var ConvertEngine = require('./lib/convert-engine')
var utils = require('./lib/utils')

module.exports = function (options) {
  options = options || {}
  options.ruleSets = utils.toArray(_.cloneDeep(options.ruleSets))
  var ruleSets = options.ruleSets
  if (options.align) ruleSets.push(extension.align)
  if (options.condense) ruleSets.push(extension.condense)
  ruleSets.push(commonmarkRuleSet)

  var engine = new ConvertEngine(ruleSets, options)

  var context = _.pick(options, [
    'lineLength',
    'align',
    'condense'
  ])

  return function () {
    // this bind to cheerio object i.e. require('cheerio').load(html)(selector)
    return S(engine.convert(this, context)).decodeHTMLEntities().s
  }
}
