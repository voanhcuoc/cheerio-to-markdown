var S = require('string')

function toArray (obj) {
  if (!obj) {
    return []
  } else if (obj instanceof Array) {
    return obj
  } else {
    return [obj]
  }
}

function trimSpace (s) {
  return s.replace(/(^ *| *$)/g, '')
}

function ensureBothEnd (s, end) {
  return S(s).ensureLeft(end).ensureRight(end).s
}

var getContent = (node, $) => S($(node).html().trim()).collapseWhitespace().s

module.exports = {
  toArray,
  trimSpace,
  ensureBothEnd,
  getContent
}
