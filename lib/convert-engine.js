var _ = require('lodash')
var utils = require('./utils')
var markdownEscape = require('markdown-escape')

function ConvertEngine (ruleSets, options) {
  this.rules = _.cloneDeep(_.flatten(_.map(ruleSets,
    (ruleSet) => (ruleSet instanceof ConvertEngine ? ruleSet.rules : ruleSet))))

  this.options = options || {}

  let _disable = []
  let _before = {}
  let _after = {}
  let _overwrite = {}

  for (let rule of this.rules) {
    let name = rule.name

    if (_disable.includes(name)) {
      rule.toDelete = true
    } else {
      let overwrite = _overwrite[name]

      rule.disable = utils.toArray(rule.disable)
      _disable = _disable.concat(rule.disable)

      rule.preHooks = _before[name] || []
      rule.postHooks = _after[name] || []
      if (overwrite) rule.replacement = overwrite.replacement

      rule.before = utils.toArray(rule.before)
      for (let hookName of rule.before) {
        _before[hookName] = utils.toArray(_before[hookName])
        _before[hookName].push(rule)
      }

      rule.after = utils.toArray(rule.after)
      for (let hookName of rule.after) {
        _after[hookName] = utils.toArray(_after[hookName])
        _after[hookName].push(rule)
      }

      rule.overwrite = utils.toArray(rule.overwrite)
      for (let hookName of rule.overwrite) {
        _overwrite[hookName] = rule
      }

      rule.filter = utils.toArray(rule.filter)
    }
  }

  this.rules = this.rules.filter(rule => !rule.toDelete)
}

function convert (root, context) {
  var result
  var $ = root.constructor
  root = $(root).clone()
  context = context || {}

  // escape markdown special characters
  for (let node of $('*', root).toArray()) {
    for (let child of node.children) {
      if (child.type === 'text') $(child).replaceWith(markdownEscape(child.data))
    }
  }
  for (let rule of this.rules) {
    rule.engine = this
    for (let filter of rule.filter) {
      if (typeof filter === 'string') {
        if (filter === ':root') {
          replace($, root, rule, context)
        } else {
          for (let node of $(filter, root).toArray()) {
            replace($, node, rule, context)
          }
        }
      }
    }
  }
  if (this.options.dropHTML) result = $.text()
  else result = $(root).html()

  return utils.trimSpace(result)
}

function apply ($, node, rule) {
  var context = node.context
  for (let hook of rule.preHooks) {
    let content = getContent(node, $)
    hook.context = context
    node = hook.replacement(content, node, $)
  }
  let content = getContent(node, $)
  rule.context = context
  var md = rule.replacement(content, node, $)
  for (let hook of rule.postHooks) {
    hook.context = context
    md = hook.replacement(md)
  }
  return md
}

function replace ($, node, rule, context) {
  node.context = _.cloneDeep(context)
  let md = apply($, node, rule)
  if (md !== undefined) {
    $(node).replaceWith(md)
  }
}

var getContent = utils.getContent

ConvertEngine.prototype.convert = convert
module.exports = ConvertEngine
