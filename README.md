# cheerio-to-markdown

This library provide method to convert directly from *cheerio object* to
*markdown string*.

*Attention:* this library is in *very early development phase*

## Install

```bash
npm install gitlab:voanhcuoc/cheerio-to-markdown#semver:0.1
```

## Usage

```javascript
var cheerio = require('cheerio')
var getConverter = require('cheerio-to-markdown')

var $ = cheerio.load(html)
$.prototype.toMarkdown = getConverter(options)

// convert all content
var body = $('body').toMarkdown()

// convert content inside a node get by selector
var content = $('#content').toMarkdown()

// convert content inside a node assigned in a variable
var list = $(listNode).toMarkdown()
```

#### Options

* `ruleSets` : an Array of combination of ruleSets and [ConvertEngine](lib/convert-engine) instance
  a ruleSet is an Array of rule, which is:

  ```javascript
  {
    name: 'rule-name',
    filter: /* selector || array of selectors */,
    /* or */ before: /* another rule name || array of rule names */,
    /* or */ after: /* another rule name || array of rule names */,
    /* or */ overwrite: /* another rule name || array of rule names */,
    replacement (content, node, $, context) {
      /* processing */
      return markdown
    }
    /* maybe */ disable: /* rule name || array of rule names to be disabled */
  }
  ```

  * `before` : this rule `replacement` will be execute before `replacement` of
    the rules specified, return node that has been altered
  * `after` : this rule `replacement` will be execute after `replacement` of
    the rules specified, take markdown as argument and return markdown altered
    ```javascript
    replacement (markdown, context) {
      /* do something */
      return markdown
    }
    ```
  * `overwrite` : this rule `replacement` will be assign to `replacement` of the
    rules specified, replace their functionality
  * `disable` : remove the specified rules out of ConvertEngine instance

* `align: true` turn on align extension
  `lineLength` that be used to align _(unstable)_
* `condense: true` turn on condense extension _(unstable)_

## Cover

* All structure in [Common Mark specification 0.28](http://spec.commonmark.org/0.28)
* Extension
  * Center-align for heading and horizontal lineLength
  * Condense newline
