require('chai').should()

describe('utils', function () {
  var utils = require('../lib/utils')
  var from = it
  describe('#toArray()', function () {
    var toArray = utils.toArray
    from('null or undefined to an empty Array', function () {
      toArray(undefined).should.deep.equal([])
      toArray(null).should.deep.equal([])
    })
    from('an Array to itself', function () {
      var a = [0, [1, 2], {a: 3}]
      toArray(a).should.deep.equal(a)
    })
    from('an Object to an Array that only contain it', function () {
      var a = {b: 1}
      toArray(a).should.deep.equal([a])
    })
  })

  describe('#trimSpace()', function () {
    it('return string with spaces but not newline being trimmed', function () {
      utils.trimSpace('   \n   trim   \n   ').should.equal('\n   trim   \n')
    })
  })

  describe('#ensureBothEnd()', function () {
    context('string-ensured already on both end', function () {
      it('should retain original string', function () {
        var s = '\nNewlines are on the left and right.\n'
        utils.ensureBothEnd(s, '\n').should.equal(s)
      })
    })
    context('string-ensured on the left', function () {
      it('should add to the right', function () {
        var s = '\nNewline is on the left.'
        utils.ensureBothEnd(s, '\n').should.equal(s + '\n')
      })
    })
    context('string-ensured on the right', function () {
      it('should add to the left', function () {
        var s = 'Newline is on the right.\n'
        utils.ensureBothEnd(s, '\n').should.equal('\n' + s)
      })
    })
    context('string-ensured not on both end', function () {
      it('should add to left and right', function () {
        var s = 'Not any newline.'
        utils.ensureBothEnd(s, '\n').should.equal('\n' + s + '\n')
      })
    })
  })

  describe('#getContent()', function () {
    var cheerio = require('cheerio')
    it('> should return trimmed innerHTML', function () {
      var $ = cheerio.load('<h1>  <em>fuck</em>  </h1>')
      var node = $('h1')[0]
      utils.getContent(node, $).should.equal('<em>fuck</em>')
    })
  })
})
