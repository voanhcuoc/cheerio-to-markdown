
### This is a markdown sample

It's very easy to make some words **bold** and other words *italic* with
Markdown. You can even [link to Google!](http://google.com)

---

```python
import this
l = list()
```

* Item 1
* Item 2
  * Item 2a
  * Item 2b

1. Item 1
1. Item 2
1. Item 3
   1. Item 3a
   1. Item 3b

![GitHub Logo](/images/logo.png)

As Kanye West said:

> We're living the future so
> the present is our past.

I think you should use an `<addr>` element here instead.
