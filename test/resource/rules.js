// These rule is just for testing, don't change except there are bugs affect
// the tests

var S = require('string')

var exports = module.exports = {}

exports.thematicBreak = {
  name: 'thematic-break',
  filter: 'hr',
  replacement (content, node, $) {
    return '\n***\n'
  }
}
exports.atxHeading = {
  name: 'atx-heading',
  filter: 'h1, h2, h3, h4, h5, h6',
  replacement (content, node, $) {
    var hashes = '#'.repeat(parseInt(node.name[1]))
    return `\n${hashes} ${content}\n`
  }
}
exports.minusThematicBreak = {
  name: 'minus-thematic-break',
  filter: 'hr',
  replacement (content, node, $) {
    return '\n---\n'
  },
  disable: exports.thematicBreak.name
}
exports.alignAtxHeading = {
  name: 'align-atx-heading',
  after: 'atx-heading',
  replacement (md) {
    var hashes = S(md.split(' ')[0]).chompLeft('\n').s
    var space = this.lineLength - hashes.length - 1
    return S(`${hashes} ${S(md.slice(hashes.length + 1).trim()).pad(space).s}`).ensureLeft('\n').ensureRight('\n').s
  },
  lineLength: 80
}
exports.smallHeading = {
  name: 'small-heading',
  before: 'atx-heading',
  replacement (content, node, $) {
    var newHeadingLevel = parseInt($(node).name[1]) + 1
    if (newHeadingLevel === 7) newHeadingLevel = 6
    $(node).name = 'h' + newHeadingLevel.toString()
    return node
  }
}

exports.removeMultipleBr = {
  name: 'remove-multiple-br',
  filter: ':root',
  replacement (content, root, $) {
    $('br+br', root).remove()
  }
}
