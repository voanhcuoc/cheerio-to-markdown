var rewire = require('rewire')
var chai = require('chai')
chai.should()
var expect = chai.expect
var S = require('string')
var cheerio = require('cheerio')

var testRules = require('./resource/rules')
var thematicBreak = testRules.thematicBreak
var atxHeading = testRules.atxHeading
var minusThematicBreak = testRules.minusThematicBreak
var alignAtxHeading = testRules.alignAtxHeading
var smallHeading = testRules.smallHeading
var removeMultipleBr = testRules.removeMultipleBr

describe('ConvertEngine', function () {
  var ConvertEngine = require('../lib/convert-engine')

  describe('#new', function () {
    context('> 1 ruleSet contain a rule', function () {
      var engine = new ConvertEngine([[thematicBreak]])
      it('> should create a new instance', function () {
        engine.should.not.equal(undefined)
      })
      it('> should retain original rule', function () {
        var rule = engine.rules[0]
        rule.name.should.equal(thematicBreak.name)
        rule.filter.should.deep.equal([thematicBreak.filter])
        rule.replacement.should.equal(thematicBreak.replacement)
      })
    })

    context('> 1 ruleSet contain 2 rules', function () {
      context('> simple rules', function () {
        var engine = new ConvertEngine([[thematicBreak, atxHeading]])
        it('> should leave order of original rules unchanged', function () {
          engine.rules[0].should.have.property('name', thematicBreak.name)
          engine.rules[1].should.have.property('name', atxHeading.name)
        })
      })

      context('> first rule `disable` second one', function () {
        var engine = new ConvertEngine([[minusThematicBreak, thematicBreak]])
        it('> should retain the first rule', function () {
          engine.rules.should.have.lengthOf(1)
          engine.rules[0].should.have.property('name', minusThematicBreak.name)
        })
      })

      context('> first rule `after` second one', function () {
        var engine = new ConvertEngine([[alignAtxHeading, atxHeading]])
        it('> should retain both rules', function () {
          engine.rules[0].should.have.property('name', alignAtxHeading.name)
          engine.rules[1].should.have.property('name', atxHeading.name)
        })
        it("> should attach first rule to second one's hook", function () {
          var postHooks = engine.rules[1].postHooks
          expect(postHooks).to.be.an('array')
          postHooks.should.have.lengthOf(1)
          postHooks[0].name.should.equal(alignAtxHeading.name)
        })
      })

      context('> first rule `before` second one', function () {
        var engine = new ConvertEngine([[smallHeading, atxHeading]])
        it('> should retain both rules', function () {
          engine.rules[0].should.have.property('name', smallHeading.name)
          engine.rules[1].should.have.property('name', atxHeading.name)
        })
        it("> should attach first rule to second one's hook", function () {
          var preHooks = engine.rules[1].preHooks
          expect(preHooks).to.be.an('array')
          preHooks.should.have.lengthOf(1)
          preHooks[0].name.should.equal(smallHeading.name)
        })
      })

      context('> first rule `overwrite` second one', function () {
        minusThematicBreak.overwrite = minusThematicBreak.disable
        delete minusThematicBreak.disable
        var engine = new ConvertEngine([[minusThematicBreak, thematicBreak]])
        it('> should assign `replacement` of first rule to the one of second rule', function () {
          var rule = engine.rules[1]
          rule.name.should.equal(thematicBreak.name)
          rule.replacement().should.equal(minusThematicBreak.replacement())
        })
      })
    })

    context('> multiple ruleSets', function () {
      var engine = new ConvertEngine([[thematicBreak], [atxHeading]])
      it('> should leave order of original rules as order of ruleSets containing them', function () {
        engine.rules[0].should.have.property('name', thematicBreak.name)
        engine.rules[1].should.have.property('name', atxHeading.name)
      })
      it('> should be the same as passing concatenation of original ruleSets', () => {
        var _engine = new ConvertEngine([[thematicBreak, atxHeading]])
        engine.rules.should.deep.equal(_engine.rules)
      })
    })

    context('> passing Engine instance', function () {
      var smallEngine = new ConvertEngine([[minusThematicBreak]])
      var bigEngine = new ConvertEngine([smallEngine, [thematicBreak]])
      it('> should be the same as passing ruleSets', function () {
        var _engine = new ConvertEngine([[minusThematicBreak], [thematicBreak]])
        bigEngine.rules.should.deep.equal(_engine.rules)
      })
    })
  })

  describe('~apply()', function () {
    var apply = rewire('../lib/convert-engine').__get__('apply')
    describe('> leaf node', function () {
      var $ = cheerio.load('<hr/>')
      var node = $('hr')[0]
      var rule = (new ConvertEngine([[thematicBreak]])).rules[0]
      it('> should return markdown', function () {
        apply($, node, rule).should.equal('\n***\n')
      })
    })

    describe('> container node', function () {
      var $ = cheerio.load('<h2>  heading  </h2>')
      var node = $('h2')[0]
      var rule = (new ConvertEngine([[atxHeading]])).rules[0]
      it('> should return markdown with appropriate content', function () {
        apply($, node, rule).should.equal('\n## heading\n')
      })
    })

    describe('> rule with postHooks', function () {
      var $ = cheerio.load('<h3>  heading  </h3>')
      var node = $('h3')[0]
      var rule = (new ConvertEngine([[alignAtxHeading, atxHeading]])).rules[1]
      it('> should return markdown with appropriate content', function () {
        apply($, node, rule).should.equal('\n### ' + S('heading').pad(76).s + '\n')
      })
    })
  })

  describe('~replace()', function () {
    var replace = rewire('../lib/convert-engine').__get__('replace')
    context('> `replacement` return markdown', function () {
      var $ = cheerio.load('<em>this<hr/>that</em>')
      var node = $('hr')[0]
      var rule = (new ConvertEngine([[thematicBreak]])).rules[0]
      it('> should replace element with markdown', function () {
        replace($, node, rule)
        $('body').html().should.equal(`<em>this${thematicBreak.replacement()}that</em>`)
      })
    })

    context('> `replacement` return undefined', function () {
      var html = '<b>this<br>that</b>'
      var $ = cheerio.load(html)
      var node = $('br')[0]
      var rule = (new ConvertEngine([[removeMultipleBr]])).rules[0]
      it('> should retain the original content', function () {
        replace($, node, rule)
        $('body').html().should.equal(html)
      })
    })
  })

  describe('#convert()', function () {
    context('> without escape', function () {
      var $ = cheerio.load('<div><h1>heading</h1><hr/></div>')
      var root = $($('div')[0])
      var engine = new ConvertEngine([[minusThematicBreak], [atxHeading, thematicBreak]])
      it('> should return markdown with appropriate content', function () {
        engine.convert(root).should.equal('\n# heading\n\n---\n')
      })
    })

    context('> with escape', function () {
      var $ = cheerio.load('<div><h1># heading #</h1></div>')
      var root = $($('div')[0])
      var engine = new ConvertEngine([[atxHeading]])
      it('> should escape special chars', function () {
        engine.convert(root).should.equal('\n# \\# heading \\#\n')
      })
    })
  })
})
