var chai = require('chai')
chai.should()
var cheerio = require('cheerio')
var getConverter = require('..')

function _match (html, md, convert, msg) {
  var $ = cheerio.load(html)
  $.prototype.convert = convert
  var main = $('body')[0]
  it(msg || '> should return markdown with appropriate content', function () {
    var result = $(main).convert()
    result.should.equal(md)
  })
}

describe('Commonmark rule set', function () {
  var convert = getConverter()
  var match = (html, md, msg) => _match(html, md, convert, msg)

  describe('rules', function () {
    describe('/atx-heading', function () {
      var html = ('<h4>\n' +
                  '  heading\n' +
                  '</h4>')
      var md = '\n#### heading\n'
      match(html, md)
    })

    describe('/thematic-break', function () {
      var html = '<hr/>'
      var md = '\n***\n'
      match(html, md)
    })

    describe('/fenced-code-block', function () {
      var html = ('<pre>\n' +
                  '<code class="language-python">\n' +
                  'import this\n' +
                  '</code>\n' +
                  '</pre>')
      var md = ('\n```python' +
                '\nimport this' +
                '\n```\n')
      match(html, md)
    })

    describe('/paragraph', function () {
      var html = '<p> This is a paragraph </p>'
      var md = '\nThis is a paragraph\n'
      match(html, md)
    })

    describe('/block-quote', function () {
      var html = '<blockquote> <h2>heading</h2><p>paragraph</p> </blockquote>'
      var md = '\n> \n> ## heading\n> \n> paragraph\n> \n\n'
      match(html, md)
    })

    describe('/ordered-list', function () {
      var html = ('<ol>' +
                  '  <li start=3>' +
                  '    Third' +
                  '  </li>' +
                  '  <li>' +
                  '    Fourth' +
                  '  </li>' +
                  '</ol>')
      var md = ('\n3. Third\n4. Fourth\n')
      match(html, md)
    })

    describe('/unordered-list', function () {
      context(':oneliner', function () {
        var html = ('<ul>' +
                    '  <li>' +
                    '    First' +
                    '  </li>' +
                    '  <li>' +
                    '    Second' +
                    '  </li>' +
                    '</ul>')
        var md = ('\n* First\n* Second\n')
        match(html, md)
      })

      context(':multiple-line item', function () {
        var html = ('<ul>' +
                    '  <li>' +
                    '    First<br/>One' +
                    '  </li>' +
                    '  <li>' +
                    '    Second<br/>Two' +
                    '  </li>' +
                    '</ul>')
        var md = '\n* First\\\n  One\n* Second\\\n  Two\n'
        match(html, md)
      })
    })

    describe('/code-span', function () {
      var html = '<code> i = 1 </code>'
      var md = '`i = 1`'
      match(html, md)
    })

    describe('/emphasis', function () {
      var html = '<em> italics </em>'
      var md = '*italics*'
      match(html, md)
    })

    describe('/strong-emphasis', function () {
      var html = '<strong> bold </strong>'
      var md = '**bold**'
      match(html, md)
    })

    describe('/link', function () {
      context(':without title', function () {
        var html = '<a href="nodejs.org"> node.js </a>'
        var md = '[node.js](nodejs.org)'
        match(html, md)
      })
      context(':with title', function () {
        var html = '<a href="nodejs.org" title="NodeJS"> node.js </a>'
        var md = '[node.js](nodejs.org "NodeJS")'
        match(html, md)
      })
    })

    describe('/image', function () {
      context(':without title', function () {
        var html = '<img src="photo.png" alt="face"/>'
        var md = '![face](photo.png)'
        match(html, md)
      })
      context(':with title', function () {
        var html = '<img src="photo.png" alt="face" title="Smiley"/>'
        var md = '![face](photo.png "Smiley")'
        match(html, md)
      })
    })

    describe('/hard-line-break', function () {
      var html = '<p> This line<br/>break here </p>'
      var md = '\nThis line\\\nbreak here\n'
      match(html, md, 'render')
    })
  })

  // describe.skip('all', function () {
  //   var marked = require('marked')
  //   var pretty = require('pretty')
  //   var fs = require('fs')
  //   var path = require('path')
  //
  //   var md = fs.readFileSync(path.join(__dirname, '/resource/sample.md')).toString()
  //   var html = marked(md)
  //
  //   describe('oneliner', function () {
  //     html = html.replace('\n', '')
  //     match(html, md)
  //   })
  //
  //   describe('HTML with newline', function () {
  //     // fs.writeFileSync(path.join(__dirname, '/resource/sample.html'), html)
  //     match(html, md)
  //   })
  //
  //   describe('indented HTML', function () {
  //     html = pretty(html)
  //     match(html, md)
  //   })
  // })
})

describe('Extended rules', function () {
  describe('align', function () {
    var lineLength = 60
    var convert = getConverter({
      align: true,
      lineLength
    })

    var match = (html, md, msg) => _match(html, md, convert, msg)

    describe('/align/atx-heading', function () {
      context('longer than lineLength', function () {
        var html = ("<h2>This is a very long heading. You know, it's so long.\n" +
                    "Damn ! What to type next ? OK, it's enough.</h2>")
        var md = ("\n## This is a very long heading. You know, it's so long. " +
                  "Damn ! What to type next ? OK, it's enough.\n")
        match(html, md, '> retain original markdown')
      })

      context('shorter than lineLength', function () {
        var html = '<h3> short text </h3>'
        var md = '\n###                        short text                       \n'
        match(html, md, '> should be center-aligned')
      })
    })

    describe('/align/thematic-break', function () {
      var html = '<hr/>'
      var md = `\n${'*'.repeat(lineLength)}\n`
      match(html, md)
    })
  })

  describe('/condense', function () {
    var convert = getConverter({
      condense: true
    })
    var match = (html, md, msg) => _match(html, md, convert, msg)
    describe('/remove-multiple-br', function () {
      var html = '<p>This is 3<br/><br/><br/>line break</p>'
      var md = '\nThis is 3\\\nline break\n'
      match(html, md)
    })
  })
})
