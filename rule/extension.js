// extended rules and ruleSets for commonmark

var _ = require('lodash')
var S = require('string')
var wordwrap = require('wordwrap')

var align = [
  {
    name: 'align/atx-heading',
    after: 'commonmark/atx-heading',
    replacement (md) {
      var hashes = this.context.hashes
      var text = this.context.text
      var spaceLeftover = this.context.lineLength - hashes.length - 1
      if (text.length > spaceLeftover) {
        // pass if text is longer than lineLength
        return md
      } else {
        return S(`${hashes} ${S(text).pad(spaceLeftover).s}`).ensureLeft('\n').ensureRight('\n').s
      }
    }
  },
  {
    name: 'align/thematic-break',
    overwrite: 'commonmark/thematic-break',
    replacement () {
      return `\n${'*'.repeat(this.context.lineLength)}\n`
    }
  }
]

var wrap = [
  {
    name: 'wrap/atx-heading',
    after: 'commonmark/atx-heading',
    replacement (md) {
      if (md.length <= this.context.lineLength) {
        return md
      } else {
        var hashes = this.context.hashes
        var wrap = wordwrap(this.context.lineLength - hashes.length - 1)
        var lines = wrap(this.context.text).split('\n')
        lines = _.map(lines, line => `${hashes} ${line}`)
        return S(lines.join('\n')).ensureLeft('\n').ensureRight('\n').s
      }
    }
  },
  {
    name: 'wrap/paragraph',
    after: 'commonmark/paragraph',
    replacement (md) {
      return wordwrap(this.context.lineLength)(md)
    }
  },
  {
    name: 'wrap/block-quote',
    before: [
      'commonmark/block-quote',
      'commonmark/ordered-list',
      'commonmark/unordered-list'
    ],
    replacement () {
      this.context.lineLength -= this.context.indent
    }
  }
]

var condense = [
  {
    name: 'remove-multiple-br',
    filter: ':root',
    replacement (content, root, $) {
      $('br+br', root).remove()
    }
  }
]

module.exports = {
  align,
  wrap,
  condense
}
