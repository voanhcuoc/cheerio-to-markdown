// rule.name follow commonmark spec:
// http://spec.commonmark.org/0.28
// using the FIRST example in each section

var S = require('string')
var _ = require('lodash')

var utils = require('../lib/utils')

var prefix = 'commonmark/'

var ensureNewline = {
  name: 'ensure-newline',
  after: [
    'atx-heading',
    'thematic-break',
    'fenced-code-block',
    'paragraph',
    'block-quote',
    'unordered-list',
    'ordered-list'
  ],
  replacement (md) {
    return utils.ensureBothEnd(md, '\n')
  }
}

var rules = [
  ensureNewline,
  /**********
  leaf blocks
  **********/
  {
    // name as in the spec, lowercase, ' ' become '-'
    name: 'atx-heading',
    filter: 'h1, h2, h3, h4, h5, h6',
    replacement (content, node) {
      var hashes = '#'.repeat(parseInt(node.name[1]))
      this.context.hashes = hashes
      this.context.text = content
      return `\n${hashes} ${content}\n`
    }
  },
  {
    name: 'thematic-break',
    filter: 'hr',
    replacement () {
      return '\n***\n'
    }
  },
  {
    name: 'fenced-code-block',
    filter: 'pre:has(code)',
    replacement (content, node, $) {
      var codeFence = '```'
      var codeTag = $(node).children('code')
      content = S(codeTag.text()).chompLeft('\n').chompRight('\n').s
      var classes = codeTag.attr('class')
      var lang
      if (classes) {
        for (let _class of classes.split(' ')) {
          if (_class.startsWith('language-')) lang = _class.slice(9)
        }
      }
      return `\n${codeFence}${lang}\n${content}\n${codeFence}\n`
    }
  },
  {
    name: 'paragraph',
    filter: 'p',
    replacement (content) {
      return `\n${content}\n`
    }
  },
  /***************
  container blocks
  ***************/
  {
    name: 'block-quote',
    filter: 'blockquote',
    replacement (content, node, $) {
      this.context.indent = 2
      var inner = this.engine.convert($(node), this.context)
      var lines = inner.split('\n')
      var outputLines = ['\n']
      for (let line of lines) {
        outputLines.push(`> ${line}\n`)
      }
      outputLines.push('\n')
      return outputLines.join('')
    }
  },
  {
    name: 'ordered-list',
    filter: 'ol',
    replacement (content, node, $) {
      var i = parseInt($('li', node).attr('start')) || 1
      var outputListItems = []
      for (let child of node.children) {
        if (child.name === 'li') {
          outputListItems.push(this._li(child, i, $))
          i++
        }
      }
      var nextTag = $($(node).next()[0]).name
      if (nextTag === 'OL' || nextTag === 'UL') {
        outputListItems.push('<!-- -->')
      }
      return `\n${outputListItems.join('\n')}\n`
    },
    _li (node, index, $) {
      var indent = ' '.repeat(index.toString().length + 2)
      this.context.indent = indent
      var inner = this.engine.convert($(node), this.context)
      var lines = inner.split('\n')
      var outputLines = [`${index}. ${lines.shift()}`]
      for (let line of lines) {
        outputLines.push(`${indent}${line}`)
      }
      return outputLines.join('\n')
    }
  },
  // separate ordered-list and unordered-list to apply hooks separately
  {
    name: 'unordered-list',
    filter: 'ul',
    replacement (content, node, $) {
      var outputListItems = []
      for (let child of node.children) {
        if (child.name === 'li') outputListItems.push(this._li(child, $))
      }
      var nextTag = $($(node).next()[0]).name
      if (nextTag === 'OL' || nextTag === 'UL') {
        outputListItems.push('<!-- -->')
      }
      return `\n${outputListItems.join('\n')}\n`
    },
    _li (node, $) {
      this.context.indent = 2
      var inner = this.engine.convert($(node), this.context)
      var lines = inner.split('\n')
      var outputLines = [`* ${lines.shift()}`]
      for (let line of lines) {
        outputLines.push(`  ${line}`)
      }
      return outputLines.join('\n')
    }
  },
  /******
  inlines
  ******/
  {
    name: 'code-span',
    filter: 'code:not(pre > code)',
    replacement (content) {
      return '`' + content + '`'
    }
  },
  {
    name: 'emphasis',
    filter: 'em, i',
    replacement (content) {
      return `*${content}*`
    }
  },
  {
    name: 'strong-emphasis',
    filter: 'strong, b',
    replacement (content) {
      return `**${content}**`
    }
  },
  {
    name: 'link',
    filter: 'a',
    replacement (content, node, $) {
      var destination = $(node).attr('href') || ''
      var title = $(node).attr('title') || ''
      title = title && ` "${title}"`
      var text = content
      return `[${text}](${destination}${title})`
    }
  },
  {
    name: 'image',
    filter: 'img',
    replacement (content, node, $) {
      var source = $(node).attr('src') || ''
      var title = $(node).attr('title') || ''
      title = title && ` "${title}"`
      var description = $(node).attr('alt')
      return `![${description}](${source}${title})`
    }
  },
  {
    // simple form, standardize context block-structure to get prefix information
    name: 'hard-line-break',
    filter: 'br',
    replacement () {
      return '\\\n'
    }
  }
]

// <prefix>
for (let rule of rules) {
  rule.name = prefix + rule.name
}
ensureNewline.after = _.map(ensureNewline.after, name => prefix + name)
// </prefix>

// TODO options

module.exports = rules
